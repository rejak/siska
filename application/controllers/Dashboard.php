<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct() {
		parent::__construct();
		checkSession();
	}

	public function index()
	{
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$levelID = $this->session->userdata['checkUsers']['Users']['userLevel'];
		$menus = $this->Menu_model->getMenu($levelID);
		$submenus = $this->Menu_model->getSubmenu($levelID);
		$requests = $this->Check_model->getRequest();
		$users = $this->Check_model->getUser();
		$username = $this->Check_model->getFindUser($userID)->row_array();

		$a = array();
		foreach ($menus->result() as $value) {
			$a[] = array('id'=>$value->id, 'name'=>$value->name, 'url'=>$value->url, 'count' => $this->Menu_model->getCount($value->id));
		}

		$data = array(
									'menu' => $menus,
									'submenu' => $submenus,
									'arr' => $a,
									'request' => $requests,
									'user' => $users,
									'username' => $username
								);
		$this->load->view('header', $data);
		if($levelID == 3){
			$this->load->view('unit/dashboard');
		}else if($levelID == 4){
			$this->load->view('pic/dashboard');
		}else if($levelID == 5){
			$this->load->view('bho/dashboard');
		}else{
			redirect('signout');
		}
		$this->load->view('dashboard');
		$this->load->view('footer');
		// $this->load->view('app_js');
	}
}
