<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Draft extends CI_Controller {
	function __construct() {
  	parent::__construct();
  	checkSession();
  }

	public function index()
	{
		$levelMenu = $this->uri->segment(1);
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$levelID = $this->session->userdata['checkUsers']['Users']['userLevel'];
		$menus = $this->Menu_model->getMenu($levelID);
		$submenus = $this->Menu_model->getSubmenu($levelID);

		$checkLevel = $this->Check_model->getCheck($levelMenu)->row_array();
		$username = $this->Check_model->getFindUser($userID)->row_array();

		$user = $this->Check_model->getUser();
		$request = $this->Request_model->getRequestPic2($userID);

		$a = array();
		foreach ($menus->result() as $value) {
			$a[] = array('id'=>$value->id, 'name'=>$value->name, 'url'=>$value->url, 'count' => $this->Menu_model->getCount($value->id));
		}

		$data = array('menu' => $menus, 'submenu' => $submenus, 'arr' => $a, 'username' => $username, 'request' => $request, 'user' => $user);

		if($checkLevel['level_id'] != $levelID){
			redirect('dashboard');
		}else{
			$this->load->view('header', $data);
			$this->load->view('pic/draft');
			$this->load->view('footer');
		}
	}

	public function detail()
	{
		$levelMenu = $this->uri->segment(1);
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$levelID = $this->session->userdata['checkUsers']['Users']['userLevel'];
		$menus = $this->Menu_model->getMenu($levelID);
		$submenus = $this->Menu_model->getSubmenu($levelID);

		$checkLevel = $this->Check_model->getCheck($levelMenu)->row_array();
		$username = $this->Check_model->getFindUser($userID)->row_array();

		$user = $this->Check_model->getUser();
		$request = $this->Request_model->getRequestPic();
		$pic = $this->User_model->getUserPIC();

		$a = array();
		foreach ($menus->result() as $value) {
			$a[] = array('id'=>$value->id, 'name'=>$value->name, 'url'=>$value->url, 'count' => $this->Menu_model->getCount($value->id));
		}

		$data = array('menu' => $menus, 'submenu' => $submenus, 'arr' => $a, 'username' => $username, 'request' => $request, 'user' => $user, 'idea' => $this->uri->segment(3), 'pic' => $pic);

		if($checkLevel['level_id'] != $levelID){
			redirect('dashboard');
		}else{
			$this->load->view('header', $data);
			$this->load->view('pic/detail');
			$this->load->view('footer');
			$this->load->view('pic/app_js');
		}
	}

	public function download_file(){
		$id  = $this->uri->segment(3);
		$file = $this->RequestDetail_model->getRequestDetailDownload($id)->row_array();
		force_download('assets/docs/'.$file['file_location'],NULL);
	}

	public function download(){
		$id  = $this->uri->segment(3);
		$file = $this->Request_model->getRequestID($id)->row_array();
		force_download('assets/docs/'.$file['file_location'],NULL);
	}

	public function viewDetail()
	{
		$id = $this->uri->segment(3);
		$request = $this->Request_model->getRequestID($id)->row_array();
		$user = $this->User_model->getUserID($request['user'])->row_array();
		$pic = $this->User_model->getUserID($request['pic'])->row_array();
		$request_detail = $this->RequestDetail_model->getRequestDetailID($request['id']);
		$users = $this->Check_model->getUser();

		echo '<div class="my-3 p-3 bg-white rounded box-shadow" >';
			echo '<br>';
			echo '<small style="padding-left:100px"><i class="far fa-calendar-alt"></i>&nbsp;'.date('l, d F Y', strtotime($request['date'])).'&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;'.date('H:i:s', strtotime($request['date'])).'</small>';
			echo '<br>';
			echo '<h5 style="padding:20px 100px 0 100px">'.$request['title'].'</h5>';
			echo '<p align="justify" style="padding:0 100px 0 100px">'.$request['description'].'</p>';
			echo '<small style="padding-left:100px"><i class="far fa-user"></i>&nbsp;'.$user['name'].'</small>';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="'.base_url().'draft/download/'.$request['id'].'"><i class="fas fa-download"></i> Download</a></small>';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="#" onclick="add_data()"><i class="fas fa-reply"></i> Reply</a></small>';
			echo '<br>';
			echo '<br>';
			if($request_detail->num_rows() > 0){
				foreach ($request_detail->result() as $value) {
					echo '<hr style="margin-bottom:0px;margin-top:0px">';
					echo '<br>';
	        echo '<small style="padding-left:100px"><i class="far fa-calendar-alt"></i>&nbsp;'.date('l, d F Y', strtotime($value->date)).'&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;'.date('H:i:s', strtotime($value->date)).'</small>';
	        echo '<br>';
					echo '<h5 style="padding:20px 100px 0 100px">'.$value->title.'</h5>';
					echo '<p align="justify" style="padding:0 100px 0 100px">'.$value->description.'</p>';
					foreach ($users->result() as $key) {
            if($key->id == $value->user){
              echo '<small style="padding-left:100px;"><i class="far fa-user"></i>&nbsp;'.$key->name.'</small>';
            }
          }
					echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="'.base_url().'draft/download_file/'.$value->id.'"><i class="fas fa-download"></i> Download</a></small>';
					echo '<div style="margin-bottom:0px">&nbsp;</div>';
				}
			}else{

			}
		echo '</div>';
	}

	function do_upload()
	{
		date_default_timezone_set("Asia/jakarta");

		$config['upload_path']="./assets/docs";
		$config['allowed_types']='gif|jpg|png|doc|docx|pdf';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		if($this->upload->do_upload("file")){
			$data = array('upload_data' => $this->upload->data());

			$kata = array(
				'title' => $this->input->post('title'),
				'description' => $this->input->post('description'),
				'file_location' => $data['upload_data']['file_name'],
				'date' => date("Y-m-d G:i:sa"),
				'user' => $this->session->userdata('checkUsers')['Users']['userID'],
				'request_id' => $this->input->post('request_id')
			);

			$result= $this->Crud_model->save($kata, 'request_details');
				echo json_decode($result);
			}
		}

}
