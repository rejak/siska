<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {
	function __construct() {
  	parent::__construct();
  	checkSession();
  }

	public function index()
	{
		$levelMenu = $this->uri->segment(1);
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$levelID = $this->session->userdata['checkUsers']['Users']['userLevel'];
		$menus = $this->Menu_model->getMenu($levelID);
		$submenus = $this->Menu_model->getSubmenu($levelID);

		$checkLevel = $this->Check_model->getCheck($levelMenu)->row_array();
		$username = $this->Check_model->getFindUser($userID)->row_array();

		$a = array();
		foreach ($menus->result() as $value) {
			$a[] = array('id'=>$value->id, 'name'=>$value->name, 'url'=>$value->url, 'count' => $this->Menu_model->getCount($value->id));
		}

		$data = array('menu' => $menus, 'submenu' => $submenus, 'arr' => $a, 'username' => $username);

		if($checkLevel['level_id'] != $levelID){
			redirect('dashboard');
		}else{
			$this->load->view('header', $data);
			$this->load->view('unit/request');
			$this->load->view('footer');
			$this->load->view('unit/app_js');
		}
	}

	public function detail()
	{
		$levelMenu = $this->uri->segment(1);
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$levelID = $this->session->userdata['checkUsers']['Users']['userLevel'];
		$menus = $this->Menu_model->getMenu($levelID);
		$submenus = $this->Menu_model->getSubmenu($levelID);

		$checkLevel = $this->Check_model->getCheck($levelMenu)->row_array();
		$username = $this->Check_model->getFindUser($userID)->row_array();

		$a = array();
		foreach ($menus->result() as $value) {
			$a[] = array('id'=>$value->id, 'name'=>$value->name, 'url'=>$value->url, 'count' => $this->Menu_model->getCount($value->id));
		}

		$data = array('menu' => $menus, 'submenu' => $submenus, 'arr' => $a, 'username' => $username, 'idea' => $this->uri->segment(3));

		if($checkLevel['level_id'] != $levelID){
			redirect('dashboard');
		}else{
			$this->load->view('header', $data);
			$this->load->view('unit/detail');
			$this->load->view('footer');
			$this->load->view('unit/app_js_detail');
		}
	}

	function do_upload()
	{
		date_default_timezone_set("Asia/jakarta");

		$config['upload_path']="./assets/docs";
		$config['allowed_types']='gif|jpg|png|doc|docx|pdf';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		if($this->upload->do_upload("file")){
			$data = array('upload_data' => $this->upload->data());

			$kata = array(
				'title' => $this->input->post('title'),
				'description' => $this->input->post('description'),
				'file_location' => $data['upload_data']['file_name'],
				'date' => date("Y-m-d G:i:sa"),
				'user' => $this->session->userdata('checkUsers')['Users']['userID']
			);

			$result= $this->Crud_model->save($kata, 'requests');
				echo json_decode($result);
			}
		}

		function do_upload_detail()
		{
			date_default_timezone_set("Asia/jakarta");

			$config['upload_path']="./assets/docs";
			$config['allowed_types']='gif|jpg|png|doc|docx|pdf';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if($this->upload->do_upload("file")){
				$data = array('upload_data' => $this->upload->data());

				$kata = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_location' => $data['upload_data']['file_name'],
					'date' => date("Y-m-d G:i:sa"),
					'user' => $this->session->userdata('checkUsers')['Users']['userID'],
					'request_id' => $this->input->post('request_id'),
					'status' => 1
				);

				$result= $this->Crud_model->save($kata, 'request_details');
					echo json_decode($result);
				}
			}

	public function download_file(){
		$id  = $this->uri->segment(3);
		$file = $this->RequestDetail_model->getRequestDetailDownload($id)->row_array();
		force_download('assets/docs/'.$file['file_location'],NULL);
	}

	public function download(){
		$id  = $this->uri->segment(3);
		$file = $this->Request_model->getRequestID($id)->row_array();
		force_download('assets/docs/'.$file['file_location'],NULL);
	}

	public function viewRequest()
	{
		$userID = $this->session->userdata['checkUsers']['Users']['userID'];
		$user = $this->Check_model->getUser();
		$request = $this->Request_model->getRequestUser($userID);

		// echo '<div class="my-3 p-3 bg-white rounded box-shadow">';
	  // echo '<h6 class="border-bottom border-gray pb-2 mb-0">Recent updates</h6>';
		if($request->num_rows() > 0){
			foreach ($request->result() as $value) {
				// echo '<div class="media text-muted pt-3">';
					// echo '<img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">';
					// echo '<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">';
						echo '<div class="my-3 p-3 bg-white rounded box-shadow">';
						echo '<br>';
						echo '<small style="padding-left:100px"><i class="far fa-calendar-alt"></i>&nbsp;'.date('l, d F Y', strtotime($value->date)).'&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;'.date('H:i:s', strtotime($value->date)).'</small>';
						echo '<br>';
						echo '<h5 style="padding:20px 100px 0 100px"><a href="'.base_url().'request/detail/'.$value->id.'">'.$value->title.'</a></h5>';
						echo '<p align="justify" style="padding:0 100px 0 100px">'.$value->description.'</p>';
						// echo '<strong class="d-block text-gray-dark"><a href="'.base_url().'request/detail/'.$value->id.'">'.$value->title.'</a></strong>';
						// echo $value->description;
						// echo '<br>';
						// echo '<br>';
						foreach ($user->result() as $key) {
							if($key->id == $value->user){
								echo '<small style="padding-left:100px"><i class="far fa-user"></i>&nbsp;'.$key->name.'</small>';
								echo '<br>';
							}
						}
					// echo '</p>';
					echo '<br>';
				echo '</div>';
			}
		}else{
			echo '<div class="my-3 p-3 bg-white rounded box-shadow">';
				echo '<div class="alert alert-warning" role="alert" style="margin-top:10px">';
					echo 'Loading ...';
				echo '</div>';
			echo '</div>';
		}

	        // echo'<small class="d-block text-right mt-3">
	          // <a href="#">All updates</a>
	        // </small>
				// echo '<br>';
	      // echo '</div>';
	}

	public function viewDetail()
	{
		$id = $this->uri->segment(3);
		$request = $this->Request_model->getRequestID($id)->row_array();
		$user = $this->User_model->getUserID($request['user'])->row_array();

		$request_detail = $this->RequestDetail_model->getRequestDetailOne($request['id']);
		$users = $this->Check_model->getUser();

		echo '<div class="my-3 p-3 bg-white rounded box-shadow" >';
			echo '<br>';
			echo '<small style="padding-left:100px"><i class="far fa-calendar-alt"></i>&nbsp;'.date('l, d F Y', strtotime($request['date'])).'&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;'.date('H:i:s', strtotime($request['date'])).'</small>';
			echo '<br>';
			echo '<h5 style="padding:20px 100px 0 100px">'.$request['title'].'</h5>';
			echo '<p align="justify" style="padding:0 100px 0 100px">'.$request['description'].'</p>';
			echo '<small style="padding-left:100px"><i class="far fa-user"></i>&nbsp;'.$user['name'].'</small>';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="'.base_url().'request/download/'.$request['id'].'"><i class="fas fa-download"></i> Download</a></small>';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="#" onclick="add_data()"><i class="fas fa-reply"></i> Reply</a></small>';
			echo '<br>';
			echo '<br>';
			if($request_detail->num_rows() > 0){
				foreach ($request_detail->result() as $value) {
					echo '<hr style="margin-bottom:0px;margin-top:0px">';
					echo '<br>';
	        echo '<small style="padding-left:100px"><i class="far fa-calendar-alt"></i>&nbsp;'.date('l, d F Y', strtotime($value->date)).'&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;'.date('H:i:s', strtotime($value->date)).'</small>';
	        echo '<br>';
					echo '<h5 style="padding:20px 100px 0 100px">'.$value->title.'</h5>';
					echo '<p align="justify" style="padding:0 100px 0 100px">'.$value->description.'</p>';
					foreach ($users->result() as $key) {
            if($key->id == $value->user){
              echo '<small style="padding-left:100px;"><i class="far fa-user"></i>&nbsp;'.$key->name.'</small>';
            }
          }
					echo '&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="'.base_url().'request/download_file/'.$value->id.'"><i class="fas fa-download"></i> Download</a></small>';
					echo '<div style="margin-bottom:0px">&nbsp;</div>';
				}
			}else{

			}
		echo '</div>';
	}

}
