
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= site_url() ?>assets/custom/offcanvas.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
    <script type="text/javascript">


      $('#addData').click(function(e){
        var title = $('#title').val(), description = $('#description').val(), text_file = $('#text_file').val();
        if(title === ""){
          alert("title harap diisi");
        }else if(description == ""){
          alert("description harap diisi");
        }else{
          // alert(title+' '+description+' '+text_file);
          e.preventDefault();
            $.ajax({
                url:'<?php echo base_url();?>request/do_upload',
                type:"post",
                data:new FormData($('#form')[0]),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                  $('#title').val("");
                  $('#description').val("")
                  $('#text_file').val("")
                    alert("Upload Image Berhasil.");
                }
            });
        }
    });
  </script>
  </body>
</html>
