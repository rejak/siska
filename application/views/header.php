
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>...</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/custom/offcanvas.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
  </head>

  <body class="bg-light">

    <nav class="navbar navbar-expand-lg fixed-top navbar-light" style="background: linear-gradient(to right, #314755, #26a0da)">
    <!-- <nav <?php //echo $username['color'] ?>> -->

      <div class="container">
      <a class="navbar-brand mr-auto mr-lg-0" href="#" style="padding-right:20px;color:#FFFFFF">
        Information Systems
      </a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <?php
          echo '<ul class="navbar-nav mr-auto">';
          for ($i=0; $i<count($arr); $i++) {
            if($arr[$i]['count'] > 0){
          //     echo '<ul>';
                echo '<li class="nav-item dropdown">';
                  echo '<a class="nav-link dropdown-toggle" href="'.base_url().''.$arr[$i]['url'].'" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$arr[$i]['name'].'</a>';
                  foreach ($submenu->result() as $value) {
                    if($value->parent_id == $arr[$i]['id']){
                      echo '<div class="dropdown-menu" aria-labelledby="dropdown01">';
                        echo '<a class="dropdown-item" href="'.base_url().''.$value->url.'">'.$value->name.'</a>';
                      echo '</div>';
                    }
                  }
                echo '</li>';
          //     echo '</ul>';
            }else{
              echo '<li class="nav-item">';
                echo '<a class="nav-link" href="'.base_url().''.$arr[$i]['url'].'">'.$arr[$i]['name'].'</a>';
              echo '</li>';
            }
          }
          echo '</ul>';
          // echo '<ul class="navbar-nav mr-auto">';
          // for($i=0; $i<count($menus); $i++){
          //   if($menus[$i][2] > 0){
          //     echo '<li class="nav-item dropdown">';
          //       echo '<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$menus[$i][1].'</a>';
          //         echo '<div class="dropdown-menu" aria-labelledby="dropdown01">';
          //           foreach ($submenus->result() as $value) {
          //             if($value->menu_id == $menus[$i][0]){
          //               echo '<a class="dropdown-item" href="'.$value->url.'">'.$value->submenu_name.'</a>';
          //             }
          //           }
          //         echo '</div>';
          //       echo '</li>';
          //   }else{
          //     echo '<li class="nav-item">
          //             <a class="nav-link" href="'.$menus[$i][2].'">'.$menus[$i][1].' <span class="sr-only">(current)</span></a>
          //           </li>';
          //   }
          // }
          // echo '</ul>';
        ?>
        <!-- <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Notifications</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Switch account</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> -->
        <ul class="navbar-nav navbar-right">
            <li class="nav-item active">
              <a class="nav-link" href="<?= base_url() ?>signout" style="color:#f5f6fa"><i class="fas fa-sign-out-alt"></i> Sign out</a>
            </li>
        </ul>
      </div>
    </div>
    </nav>

    <div class="nav-scroller bg-white box-shadow">
      <div class="container">
      <nav class="nav nav-underline">
        <a class="nav-link active" href="#"><i class="fas fa-user-circle"></i>&nbsp;<?= $username['name'] ?></a>
        <a class="nav-link" href="#"><i class="fas fa-bolt"></i>&nbsp;<?= $username['nama_level'] ?></a>
        <!-- <a class="nav-link" href="#">
          Friends
          <span class="badge badge-pill bg-light align-text-bottom"></span>
        </a>
        <a class="nav-link" href="#">Explore</a>
        <a class="nav-link" href="#">Suggestions</a>
        <a class="nav-link" href="<?= base_url() ?>signout">Sign Out</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a> -->
      </nav>
      </div>
    </div>
    <?php
      // print_r($ab);
      // echo '<br>';
      // for($i=0; $i<count($menus); $i++){
      //   echo '<ul>';
      //   echo '<li>'.$menus[$i][0].' '.$menus[$i][1].' '.$menus[$i][2];
      //   if($menus[$i][2] > 0){
      //     echo '<ul>';
      //     foreach ($submenus->result() as $value) {
      //       if($value->menu_id == $menus[$i][0]){
      //         echo '<li>'.$value->submenu_name.'</li>';
      //       }
      //     }
      //     echo '</ul>';
      //   }
      //   echo '</li>';
      //   echo '</ul>';
      // }
    ?>
