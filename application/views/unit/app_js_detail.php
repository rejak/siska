<script type="text/javascript">

$('#viewDetail').load('<?php echo base_url();?>request/viewDetail/<?php echo $idea; ?>');

function add_data()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#exampleModal').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Data'); // Set Title to Bootstrap modal title
}

function save()
{
  // e.preventDefault();
    $.ajax({
        url:'<?php echo base_url();?>request/do_upload_detail',
        type:"post",
        data:new FormData($('#form')[0]),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success: function(data){
          // $('#title').val("");
          // $('#description').val("")
          // $('#text_file').val("")
          //   alert("Upload Image Berhasil.");
          $('#exampleModal').modal('hide');
          $('#viewDetail').load('<?php echo base_url();?>request/viewDetail/<?php echo $idea; ?>');
        }
    });
}

</script>
