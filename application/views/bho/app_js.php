<script>
  $('#viewListRequest').load('<?php echo base_url();?>listRequest/viewListRequest');

  function add_pic(id)
  {
    save_method = 'add';
    $('#formAddPic')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#exampleModalPic').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Data'); // Set Title to Bootstrap modal title
    $('#request_id').val(''+id)
  }

  function save_pic()
  {
    // e.preventDefault();
      $.ajax({
          url:'<?php echo base_url();?>listRequest/save_pic',
          type:"post",
          data:new FormData($('#formAddPic')[0]),
          processData:false,
          contentType:false,
          cache:false,
          async:false,
          success: function(data){
            // $('#title').val("");
            // $('#description').val("")
            // $('#text_file').val("")
            //   alert("Upload Image Berhasil.");
            $('#exampleModalPic').modal('hide');
            alertify.success('Successfully');
            $('#viewListRequest').load('<?php echo base_url();?>listRequest/viewListRequest');
          }
      });
  }
</script>
