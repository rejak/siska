<main role="main" class="container">
  <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
    <img class="mr-3" src="<?= base_url() ?>assets/img/script.svg" alt="" width="48" height="48">
    <div class="lh-100">
      <h6 class="mb-0 text-white lh-100">Bootstrap</h6>
      <small>Since 2011</small>
    </div>
  </div>
  <div id="viewListRequest"></div>
</main>
<div class="modal fade" id="exampleModalPic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formAddPic">
          <div class="form-group">
            <label class="col-form-label">PIC Name</label>
            <input type="hidden" name="request_id" id="request_id" class="form-control">
            <select class="form-control" name="selectPICName" id="selectPICName">
              <option value="0">-</option>
              <?php
                foreach ($pic->result() as $value) {
              ?>
              <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
              <?php
                }
              ?>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="save_pic()">Save changes</button>
      </div>
    </div>
  </div>
</div>
