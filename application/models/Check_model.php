<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_Model extends CI_Model {

  public function getCheck($id)
  {
    return $this->db->get_where('tries', array('url' => $id));
  }

  public function getRequest()
  {
    $this->db->order_by('date', 'DESC');
    return $this->db->get('requests');
  }

  public function getUser()
  {
    return $this->db->get('users');
  }

  public function getFindUser($id)
  {
    $this->db->select('users.id, users.name, users.email, users.level_id, users.active, levels.nama_level, users.color');
    $this->db->from('users');
    $this->db->join('levels', 'levels.id = users.level_id');
    $this->db->where('users.id', $id);
    return $this->db->get();
  }

  public function getFindRequest($id)
  {
    return $this->db->get_where('requests', array('id' => $id));
  }

  public function getFindRequestDetail($id)
  {
    $this->db->order_by('date', 'ASC');
    return $this->db->get_where('request_details', array('request_id' => $id));
  }

  public function getPIC()
  {
    return $this->db->get_where('users', array('level_id' => 4));
  }

  public function getRequestPIC($id)
  {
    $this->db->order_by('date', 'DESC');
    return $this->db->get_where('requests', array('pic' => $id));
  }

  public function getDownloadSubdetail($id)
  {
    return $this->db->get_where('request_details', array('id' => $id));
  }

  public function getDownloaddetail($id)
  {
    return $this->db->get_where('requests', array('id' => $id));
  }

}
