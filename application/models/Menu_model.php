<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_Model extends CI_Model {

	public function getMenu($id){
    return $this->db->get_where('tries', array('level_id' => $id, 'category' => 'menu'));
  }

  public function getSubmenu($id){
    return $this->db->get_where('tries', array('level_id' => $id, 'category' => 'submenu'));
  }

  public function getCount($id){
    $submenu = $this->db->get_where('tries', array('parent_id' => $id, 'category' => 'submenu'));
    return $submenu->num_rows();
  }

}
