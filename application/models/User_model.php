<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function getUserID($id)
	{
		return $this->db->get_where('users', array('id' => $id));
	}

	public function getUserPIC()
	{
		return $this->db->get_where('users', array('level_id' => 4));
	}

}
