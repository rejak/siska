<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_Model extends CI_Model {

	public function getAll($id){
		return $this->db->get_where('tries', array('level_id' => $id));
	}

  public function getCount($id){
    $count = $this->db->get_where('submenus',array('menu_id' => $id));
    return $count->num_rows();
  }

  public function getWhere($id){
    return $this->db->get_where('submenus',array('menu_id' => $id));
  }

  public function getSubmenu(){
		return $this->db->get('submenus');
	}

	public function save($data, $table)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($id, $data, $table)
	{
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}

}
