<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestDetail_model extends CI_Model {

	public function getRequestDetailID($id)
	{
		return $this->db->get_where('request_details', array('request_id' => $id));
	}

	public function getRequestDetailOne($id)
	{
		return $this->db->get_where('request_details', array('request_id' => $id, 'status' => 1));
	}

	public function getRequestDetailDownload($id)
	{
		return $this->db->get_where('request_details', array('id' => $id));
	}

}
