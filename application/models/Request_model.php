<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request_model extends CI_Model {

	public function getRequestID($id)
	{
		return $this->db->get_where('requests', array('id' => $id));
	}

	public function getRequestUser($id)
  {
		$this->db->order_by('date', 'DESC');
    return $this->db->get_where('requests', array('user' => $id));
  }

	public function getRequestPic()
	{
		$this->db->order_by('date', 'DESC');
		return $this->db->get_where('requests', array('pic' => 0));
	}

	public function getRequestPic1()
	{
		$this->db->order_by('date', 'DESC');
		return $this->db->get_where('requests', array('pic !=' => 0));
	}

	public function getRequestPic2($id)
	{
		return $this->db->get_where('requests', array('pic' => $id));
	}

}
